FROM bitnami/opencart:3

# Fix port issue
COPY ./config/libopencart.sh /opt/bitnami/scripts/libopencart.sh

# Add theme to opencart
COPY ./themes/Fashion/ /opt/bitnami/opencart

# Overwrite entrypoint
COPY scripts/entrypoint.sh /opt/bitnami/scripts/opencart/entrypoint.sh

USER root

RUN chmod +x /opt/bitnami/scripts/opencart/entrypoint.sh
RUN ln -s /opt/bitnami/scripts/opencart/entrypoint.sh /

ENTRYPOINT ["./entrypoint.sh", "/opt/bitnami/scripts/apache/run.sh"]
